package com.silvioapps.layoutsample.features.shared.idling_resources

import androidx.test.espresso.IdlingResource
import com.silvioapps.layoutsample.features.shared.listeners.FetcherListener
import javax.inject.Inject

class FetchingIdlingResource @Inject constructor(): IdlingResource, FetcherListener {
    private lateinit var resourceCallback: IdlingResource.ResourceCallback
    private var isIdle = false

    override fun getName(): String {
        return FetchingIdlingResource::class.java.simpleName
    }

    override fun isIdleNow() = isIdle

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        resourceCallback = callback
    }

    override fun beginFetching() {
        isIdle = false
    }

    override fun doneFetching() {
        isIdle = true
        resourceCallback.onTransitionToIdle()
    }
}