package com.silvioapps.layoutsample.features.list

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.silvioapps.layoutsample.R
import com.silvioapps.layoutsample.di.applications.App
import com.silvioapps.layoutsample.di.components.DaggerAppComponent
import com.silvioapps.layoutsample.di.modules.MainFragmentModule
import com.silvioapps.layoutsample.di.modules.RecyclerViewModule
import com.silvioapps.layoutsample.features.list.activities.MainActivity
import org.junit.Before
import org.junit.Rule
import org.robolectric.annotation.LooperMode
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.contrib.RecyclerViewActions
import com.silvioapps.layoutsample.features.shared.idling_resources.FetchingIdlingResource
import com.silvioapps.layoutsample.features.shared.matchers.CustomMatchers.Companion.atPosition
import org.hamcrest.Matchers.allOf
import org.junit.After

@RunWith(AndroidJUnit4::class)
@LargeTest
@LooperMode(LooperMode.Mode.PAUSED)
class MainFragmentUnitTest {
    @get:Rule val activityTestRule = ActivityTestRule(MainActivity::class.java, true, true)
    private var fetchingIdlingResource = FetchingIdlingResource()

    @Before
    fun before(){
        val app = InstrumentationRegistry.getInstrumentation().context as App
        DaggerAppComponent
            .builder()
            .application(app)
            .mainFragmentModule(MainFragmentModule())
            .recyclerViewModule(RecyclerViewModule())
            .build()
            .inject(app)

        IdlingRegistry.getInstance().register(fetchingIdlingResource)
        activityTestRule.activity.mainFragment.fetcherListener = fetchingIdlingResource
    }

    @After
    fun after() {
        IdlingRegistry.getInstance().unregister(fetchingIdlingResource)
    }

    @Test
    fun all(){
        onView(withId(R.id.titleTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.spotlightCardView)).check(matches(isDisplayed()))

        var position = 0

        onView(withId(R.id.spotlightRecyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.imageView), isDisplayed())))))

        onView(withId(R.id.spotlightRecyclerView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(position, click()))

        position = activityTestRule.activity.mainFragment.spotlightListAdapter.itemCount - 1

        onView(withId(R.id.spotlightRecyclerView)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(position))

        onView(withId(R.id.spotlightRecyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.imageView), isDisplayed())))))

        onView(withId(R.id.cashTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.cashTextView)).check(matches(withText("digio Cash")))

        onView(withId(R.id.cashCardView)).check(matches(isDisplayed()))

        onView(withId(R.id.scrollView)).perform(swipeUp())

        onView(withId(R.id.productsTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.productsRecyclerView)).check(matches(isDisplayed()))

        position = 0

        onView(withId(R.id.productsRecyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.imageView), isDisplayed())))))

        onView(withId(R.id.productsRecyclerView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(position, click()))

        position = activityTestRule.activity.mainFragment.productsListAdapter.itemCount - 1

        onView(withId(R.id.productsRecyclerView)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(position))

        onView(withId(R.id.productsRecyclerView)).check(matches(
            atPosition(position, hasDescendant(allOf(withId(R.id.imageView), isDisplayed())))))
    }
}
