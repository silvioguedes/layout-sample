package com.silvioapps.layoutsample.features.list

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.silvioapps.layoutsample.R
import com.silvioapps.layoutsample.di.applications.App
import com.silvioapps.layoutsample.di.components.DaggerAppComponent
import com.silvioapps.layoutsample.di.modules.MainFragmentModule
import com.silvioapps.layoutsample.di.modules.RecyclerViewModule
import com.silvioapps.layoutsample.features.list.activities.MainActivity
import org.junit.Before
import org.junit.Rule
import org.robolectric.annotation.LooperMode

@RunWith(AndroidJUnit4::class)
@LargeTest
@LooperMode(LooperMode.Mode.PAUSED)
class MainActivityUnitTest {
    @get:Rule val activityTestRule = ActivityTestRule(MainActivity::class.java, true, true)

    @Before
    fun before(){
        val app = InstrumentationRegistry.getInstrumentation().context as App
        DaggerAppComponent
            .builder()
            .application(app)
            .mainFragmentModule(MainFragmentModule())
            .recyclerViewModule(RecyclerViewModule())
            .build()
            .inject(app)
    }

    @Test
    fun fragmentContainerView_isDisplayed() {
        onView(withId(R.id.fragmentContainerView)).check(matches(isDisplayed()))
    }
}
