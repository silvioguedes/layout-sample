package com.silvioapps.layoutsample.constants

class Constants {
    companion object {
        const val API_BASE_URL = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/"
        const val LIST = "sandbox/products"
        const val TIMEOUT : Long = 15
    }
}
