package com.silvioapps.layoutsample.di.modules

import com.silvioapps.layoutsample.features.list.adapters.ProductsListAdapter
import com.silvioapps.layoutsample.features.list.adapters.SpotlightListAdapter
import com.silvioapps.layoutsample.features.list.fragments.MainFragment
import com.silvioapps.layoutsample.features.list.models.ProductsItem
import com.silvioapps.layoutsample.features.list.models.SpotlightItem
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class MainFragmentModule{

    @Provides
    open fun providesMutableListOfSpotlightItem(): MutableList<SpotlightItem>{
        return mutableListOf<SpotlightItem>()
    }

    @Provides
    open fun providesSpotlightListAdapter(list: MutableList<SpotlightItem>, viewClickListener : MainFragment.SpotlightListViewClickListener, callback: MainFragment.ListPicassoCallbackListener): SpotlightListAdapter {
        return SpotlightListAdapter(list, viewClickListener, callback)
    }

    @Provides
    @Singleton
    open fun providesSpotlightListViewClickListener(): MainFragment.SpotlightListViewClickListener{
        return MainFragment.SpotlightListViewClickListener.SpotlightListViewClickListenerImpl()
    }

    @Provides
    open fun providesMutableListOfProductsItem(): MutableList<ProductsItem>{
        return mutableListOf<ProductsItem>()
    }

    @Provides
    open fun providesProductsListAdapter(list: MutableList<ProductsItem>, viewClickListener : MainFragment.ProductsListViewClickListener, callback: MainFragment.ListPicassoCallbackListener): ProductsListAdapter {
        return ProductsListAdapter(list, viewClickListener, callback)
    }

    @Provides
    @Singleton
    open fun providesProductsListViewClickListener(): MainFragment.ProductsListViewClickListener{
        return MainFragment.ProductsListViewClickListener.ProductsListViewClickListenerImpl()
    }

    @Provides
    open fun providesListPicassoCallbackListener(): MainFragment.ListPicassoCallbackListener {
        return MainFragment.ListPicassoCallbackListener.ListPicassoCallbackListenerImpl()
    }
}