package com.silvioapps.layoutsample.di.modules

import android.content.Context
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.RecyclerView
import com.silvioapps.layoutsample.R
import com.silvioapps.layoutsample.constants.Constants
import com.silvioapps.layoutsample.features.list.fragments.MainFragment
import com.silvioapps.layoutsample.features.shared.utils.Utils
import dagger.Module
import dagger.Provides

@Module
open class RecyclerViewModule{

    @Provides
    fun providesSpotlightLinearLayoutManager(context: Context): MainFragment.SpotlightLinearLayoutManager {
        return object: MainFragment.SpotlightLinearLayoutManager(context, HORIZONTAL, false){
            override fun checkLayoutParams(layoutParams: RecyclerView.LayoutParams): Boolean {
                layoutParams.height = (Utils.getScreenSize(context).y / ResourcesCompat.getFloat(context.resources, R.dimen.row_size_percent)).toInt()
                return true
            }
        }
    }

    @Provides
    fun providesProductsLinearLayoutManager(context: Context): MainFragment.ProductsLinearLayoutManager {
        return object: MainFragment.ProductsLinearLayoutManager(context, HORIZONTAL, false){
            override fun checkLayoutParams(layoutParams: RecyclerView.LayoutParams): Boolean {
                layoutParams.width = (Utils.getScreenSize(context).x / ResourcesCompat.getFloat(context.resources, R.dimen.column_size_percent)).toInt()
                layoutParams.height = (Utils.getScreenSize(context).y / ResourcesCompat.getFloat(context.resources, R.dimen.row_size_percent)).toInt()
                return true
            }
        }
    }

    @Provides
    fun providesDefaultItemAnimator(): DefaultItemAnimator {
        return DefaultItemAnimator()
    }
}