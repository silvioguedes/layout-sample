package com.silvioapps.layoutsample.di.modules

import com.silvioapps.layoutsample.features.list.fragments.MainFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {
    @ContributesAndroidInjector
    abstract fun contributesMainFragment(): MainFragment
}
