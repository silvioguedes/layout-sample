package com.silvioapps.layoutsample.di.components

import android.app.Application
import com.silvioapps.layoutsample.di.applications.App
import com.silvioapps.layoutsample.di.modules.AppModule
import com.silvioapps.layoutsample.di.modules.MainFragmentModule
import com.silvioapps.layoutsample.di.modules.RecyclerViewModule
import javax.inject.Singleton
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Singleton
@Component(modules = [AndroidInjectionModule::class, AndroidSupportInjectionModule::class, AppModule::class,
    RecyclerViewModule::class, MainFragmentModule::class])
interface AppComponent: AndroidInjector<Any>{

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application): Builder
        fun recyclerViewModule(recyclerViewModule: RecyclerViewModule): Builder
        fun mainFragmentModule(mainFragmentModule: MainFragmentModule): Builder
        fun build(): AppComponent
    }

    fun inject(application: App)
}
