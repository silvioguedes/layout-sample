package com.silvioapps.layoutsample.di.modules

import com.silvioapps.layoutsample.features.list.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {
    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity
}
