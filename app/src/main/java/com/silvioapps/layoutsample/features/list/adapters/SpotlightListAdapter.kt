package com.silvioapps.layoutsample.features.list.adapters

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.silvioapps.layoutsample.BR
import com.silvioapps.layoutsample.R
import com.silvioapps.layoutsample.features.list.fragments.MainFragment
import com.silvioapps.layoutsample.features.list.models.SpotlightItem
import com.silvioapps.layoutsample.features.shared.listeners.PicassoCallbackListener
import com.silvioapps.layoutsample.features.shared.utils.Utils
import com.silvioapps.movieslist.features.shared.listeners.ViewClickListener

class SpotlightListAdapter (list_ : MutableList<SpotlightItem>, viewClickListener_ : MainFragment.SpotlightListViewClickListener, callback_: PicassoCallbackListener) : RecyclerView.Adapter<SpotlightListAdapter.BindingViewHolder>() {
    private var list: MutableList<SpotlightItem>

    companion object{
        private lateinit var viewClickListener: ViewClickListener
        private lateinit var callback: PicassoCallbackListener
    }

    init{
        this.list = list_
        viewClickListener = viewClickListener_
        callback = callback_
    }

    class BindingViewHolder(view : View, list: MutableList<SpotlightItem>) : RecyclerView.ViewHolder(view){
        var viewDataBinding : ViewDataBinding? = null

        init{
            viewDataBinding = DataBindingUtil.bind<ViewDataBinding>(view)
            Utils.setClickListeners(view, viewClickListener, list)
        }
    }

    override fun onCreateViewHolder(parent : ViewGroup, viewType: Int) : BindingViewHolder{
        val view = LayoutInflater.from(parent.context).inflate(R.layout.spotlight_list_layout, parent, false)
        return BindingViewHolder(view, list)
    }

    override fun onBindViewHolder(holder : BindingViewHolder, position : Int) {
        Utils.setTags(position, holder.itemView)

        if(list.size > position) {
            val spotlightItem = list.get(position)
            holder.viewDataBinding?.setVariable(BR.callback, callback)
            holder.viewDataBinding?.setVariable(BR.spotlight, spotlightItem)
            holder.viewDataBinding?.executePendingBindings()
        }
    }

    override fun getItemCount() : Int{
        return list.size
    }

    fun getList(): MutableList<SpotlightItem>{
        return list
    }
}
