package com.silvioapps.layoutsample.features.shared.listeners

import android.widget.ImageView

interface PicassoCallbackListener {
    fun onSuccess(imageView: ImageView)
    fun onError(imageView: ImageView)
}