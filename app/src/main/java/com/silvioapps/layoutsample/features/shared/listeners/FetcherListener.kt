package com.silvioapps.layoutsample.features.shared.listeners

interface FetcherListener {
    fun beginFetching()
    fun doneFetching()
}