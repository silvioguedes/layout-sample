package com.silvioapps.layoutsample.features.list.services

import com.silvioapps.layoutsample.constants.Constants
import com.silvioapps.layoutsample.features.list.models.Response
import io.reactivex.Observable
import retrofit2.http.GET

interface ListService {
    @GET(Constants.LIST)
    fun getList() : Observable<Response>
}