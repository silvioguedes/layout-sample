package com.silvioapps.layoutsample.features.list.models

import java.io.Serializable
import javax.inject.Singleton

@Singleton
data class SpotlightItem(
	val name: String? = null,
	val bannerURL: String? = null,
	val description: String? = null
): Serializable
