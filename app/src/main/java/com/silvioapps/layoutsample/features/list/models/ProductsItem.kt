package com.silvioapps.layoutsample.features.list.models

import java.io.Serializable
import javax.inject.Singleton

@Singleton
data class ProductsItem(
	val imageURL: String? = null,
	val name: String? = null,
	val description: String? = null
): Serializable
