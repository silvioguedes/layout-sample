package com.silvioapps.layoutsample.features.shared.utils

import android.content.Context
import android.graphics.Point
import android.view.View
import android.view.ViewGroup
import com.silvioapps.movieslist.features.shared.listeners.ViewClickListener

class Utils {
    companion object{
        fun setTags(position : Int, view : View) {
            if (view is ViewGroup) {
                for (index in 0..view.getChildCount()) {
                    val nextChild : View? = view.getChildAt(index)
                    nextChild?.setTag(position)

                    if (nextChild is ViewGroup) {
                        setTags(position, nextChild)
                    }
                }
            }
        }

        fun setClickListeners(view : View, viewClickListener : ViewClickListener?, list: List<Any>) {
            if (view is ViewGroup) {
                for (index in 0..view.getChildCount()) {
                    val nextChild : View? = view.getChildAt(index)
                    nextChild?.setOnClickListener(object : View.OnClickListener {
                        override fun onClick(v : View) {
                            if (nextChild.getTag() != null) {
                                viewClickListener?.onClick(nextChild.context, nextChild, nextChild.getTag() as Int, list)
                            }
                        }
                    });

                    if (nextChild is ViewGroup) {
                        setClickListeners(nextChild, viewClickListener, list)
                    }
                }
            }
        }

        fun getScreenSize(context: Context): Point {
            val metrics = context.getResources().getDisplayMetrics()
            val width = metrics.widthPixels
            val height = metrics.heightPixels
            return Point(width, height)
        }
    }
}
