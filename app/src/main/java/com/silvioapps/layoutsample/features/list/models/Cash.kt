package com.silvioapps.layoutsample.features.list.models

import java.io.Serializable
import javax.inject.Singleton

@Singleton
data class Cash(
	val bannerURL: String? = null,
	val description: String? = null,
	val title: String? = null
): Serializable
