package com.silvioapps.layoutsample.features.list.models

import java.io.Serializable
import javax.inject.Singleton

@Singleton
data class Response(
    val cash: Cash? = null,
    val spotlight: List<SpotlightItem>? = null,
    val products: List<ProductsItem>? = null
): Serializable
