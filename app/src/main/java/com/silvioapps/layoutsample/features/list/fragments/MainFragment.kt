package com.silvioapps.layoutsample.features.list.fragments

import android.annotation.SuppressLint
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.silvioapps.layoutsample.R
import com.silvioapps.layoutsample.constants.Constants
import com.silvioapps.layoutsample.databinding.FragmentMainBinding
import com.silvioapps.layoutsample.features.list.adapters.ProductsListAdapter
import com.silvioapps.layoutsample.features.list.adapters.SpotlightListAdapter
import com.silvioapps.layoutsample.features.list.models.ProductsItem
import com.silvioapps.layoutsample.features.list.models.Response
import com.silvioapps.layoutsample.features.list.models.SpotlightItem
import com.silvioapps.layoutsample.features.shared.services.ServiceGenerator
import com.silvioapps.layoutsample.features.list.services.ListService
import com.silvioapps.layoutsample.features.shared.fragments.CustomFragment
import com.silvioapps.layoutsample.features.shared.listeners.FetcherListener
import com.silvioapps.layoutsample.features.shared.listeners.PicassoCallbackListener
import com.silvioapps.layoutsample.features.shared.utils.Utils
import com.silvioapps.movieslist.features.shared.listeners.ViewClickListener
import com.squareup.picasso.Picasso
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.Serializable
import javax.inject.Inject

class MainFragment @Inject constructor(): CustomFragment() {
    @Inject lateinit var spotlightListAdapter: SpotlightListAdapter
    @Inject lateinit var productsListAdapter: ProductsListAdapter
    private lateinit var fragmentMainBinding: FragmentMainBinding
    @Inject lateinit var context_: Context
    @Inject lateinit var spotlightLinearLayoutManager: SpotlightLinearLayoutManager
    @Inject lateinit var productsLinearLayoutManager: ProductsLinearLayoutManager
    @Inject lateinit var defaultItemAnimator: DefaultItemAnimator
    lateinit var fetcherListener: FetcherListener
    private lateinit var response_: Response

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(layoutInflater : LayoutInflater, viewGroup : ViewGroup?, bundle : Bundle?) : View?{
        fragmentMainBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_main, viewGroup, false)
        fragmentMainBinding.progressBar.setVisibility(View.VISIBLE)

        fragmentMainBinding.spotlightRecyclerView.apply{
            layoutManager = spotlightLinearLayoutManager
            itemAnimator = defaultItemAnimator
            setHasFixedSize(true)
            adapter = spotlightListAdapter
        }

        fragmentMainBinding.productsRecyclerView.apply{
            layoutManager = productsLinearLayoutManager
            itemAnimator = defaultItemAnimator
            setHasFixedSize(true)
            adapter = productsListAdapter
        }

        fetcherListener = ListFetcherListenerImpl()

        if(bundle != null){
            @Suppress("UNCHECKED_CAST")
            setResponse(bundle.getSerializable("response") as Response)
        }
        else{
            loadMore()
        }

        return fragmentMainBinding.root
    }

    override fun onAttach(context: Context){
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onSaveInstanceState(outState : Bundle) {
        outState.putSerializable("response", response_ as Serializable)
    }

    interface ListPicassoCallbackListener : PicassoCallbackListener{
        override fun onSuccess(imageView: ImageView) {}
        override fun onError(imageView: ImageView) {}

        class ListPicassoCallbackListenerImpl @Inject constructor(): ListPicassoCallbackListener{
            override fun onSuccess(imageView: ImageView) {}

            override fun onError(imageView: ImageView) {
                Picasso.with(imageView.context).load(R.drawable.no_image_available).into(imageView)
            }
        }
    }

    interface SpotlightListViewClickListener : ViewClickListener {
        override fun onClick(context : Context, view : View, position : Int, list: List<Any>)

        class SpotlightListViewClickListenerImpl @Inject constructor(): SpotlightListViewClickListener{
            override fun onClick(context: Context, view: View, position: Int, list: List<Any>) {
                Toast.makeText(context, (list[position] as SpotlightItem).description, Toast.LENGTH_LONG).show()
            }
        }
    }

    interface ProductsListViewClickListener : ViewClickListener {
        override fun onClick(context : Context, view : View, position : Int, list: List<Any>)

        class ProductsListViewClickListenerImpl @Inject constructor(): ProductsListViewClickListener{
            override fun onClick(context: Context, view: View, position: Int, list: List<Any>) {
                Toast.makeText(context, (list[position] as ProductsItem).description, Toast.LENGTH_LONG).show()
            }
        }
    }

    class ListFetcherListenerImpl: FetcherListener{
        override fun beginFetching(){}
        override fun doneFetching(){}
    }

    open class ProductsLinearLayoutManager(context: Context, orientation: Int, reverseLayout: Boolean) : LinearLayoutManager(context, orientation, reverseLayout)
    open class SpotlightLinearLayoutManager(context: Context, orientation: Int, reverseLayout: Boolean) : LinearLayoutManager(context, orientation, reverseLayout)

    @SuppressLint("CheckResult")
    protected fun loadMore() {
        val service : ListService = ServiceGenerator.createService(Constants.API_BASE_URL, Constants.TIMEOUT, ListService::class.java)
        service.getList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {fetcherListener.beginFetching()}
            .doFinally {fetcherListener.doneFetching()}
            .subscribe (
                {setResponse(it)},
                {Toast.makeText(context_, getString(R.string.list_error), Toast.LENGTH_LONG).show()}
            )
    }

    protected fun setResponse(values : Response){
        response_ = values

        fragmentMainBinding.cash = values.cash

        spotlightListAdapter.getList().addAll(values.spotlight as List<SpotlightItem>)
        spotlightListAdapter.notifyDataSetChanged()

        productsListAdapter.getList().addAll(values.products as List<ProductsItem>)
        productsListAdapter.notifyDataSetChanged()

        fragmentMainBinding.progressBar.setVisibility(View.GONE)
    }
}
