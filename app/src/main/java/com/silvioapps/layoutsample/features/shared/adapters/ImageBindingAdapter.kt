package com.silvioapps.layoutsample.features.shared.adapters

import androidx.databinding.BindingAdapter
import android.widget.ImageView
import com.silvioapps.layoutsample.features.shared.listeners.PicassoCallbackListener
import com.squareup.picasso.Callback

import com.squareup.picasso.Picasso

@BindingAdapter(value=["android:url", "callback"], requireAll = false)
fun setImage(imageView : ImageView, url : String?, callback: PicassoCallbackListener?) {
    Picasso.with(imageView.context).load(url).into(imageView, object : Callback {
        override fun onSuccess() {
            callback?.onSuccess(imageView)
        }

        override fun onError() {
            callback?.onError(imageView)
        }
    })
}

